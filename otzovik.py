import json
import inspect
import logging
import random
import string
import sys
import time
import traceback

import requests
from bs4 import BeautifulSoup
from lxml.html import fromstring

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

fh = logging.FileHandler('logs.log')
fh.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(levelname)-8s | %(asctime)s | %(message)s')

ch.setFormatter(formatter)
fh.setFormatter(formatter)

logger.addHandler(ch)
logger.addHandler(fh)


class Browser:
    """
    Object for parsing web pages.
    """
    def __init__(self, max_proxy_update_retries=10, get_proxies_from_web=True):
        print(get_proxies_from_web)
        self.get_proxies_from_web = get_proxies_from_web
        self.max_proxy_update_retries = max_proxy_update_retries
        self.headers_base = {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "Referer": "https://www.google.com",
            "Sec-Fetch-Mode": "navigate",
            "Sec-Fetch-Site": "cross-site",
            "Sec-Fetch-User": "?1",
            "Upgrade-Insecure-Requests": "1",
        }
        self.current_proxy = None
        self.broken_proxies = []
        self._proxies = None
        self._user_agents = None

    def update_proxies(self):
        """Get new proxies from the web or from file `proxy`"""
        if self.get_proxies_from_web:
            url = 'https://free-proxy-list.net/'
            response = requests.get(url)
            parser = fromstring(response.text)
            proxies = []
            for row in parser.xpath('//tbody/tr')[:10]:
                if row.xpath('.//td[7][contains(text(),"yes")]'):
                    proxy = (row.xpath('.//td[1]/text()')[0], row.xpath('.//td[2]/text()')[0])
                    proxies.append(proxy)

        else:
            with open('proxies.txt') as fo:
                proxies = [
                    tuple(proxy.strip('\n').split(':'))
                    for proxy in fo.readlines()
                ]

        self._proxies = proxies

    @property
    def proxies(self):
        if not self._proxies:

            self.update_proxies()

        return self._proxies

    @property
    def user_agents(self):
        if not self._user_agents:
            with open('user_agents.txt') as fo:
                self._user_agents = [ua.strip('\n') for ua in fo.readlines()]

        return self._user_agents

    def choose_user_agent(self):
        user_agents = self.user_agents
        return random.choice(user_agents)

    def choose_proxy(self):
        """Choose proxy from proxies pool and check that proxy is working properly."""
        for attempt in range(self.max_proxy_update_retries):
            for proxy in self.proxies:

                if proxy in self.broken_proxies:
                    continue

                if proxy_is_working(proxy):
                    self.current_proxy = proxy
                    return proxy

                logger.debug(f'Remove proxy {proxy[0]}:{proxy[1]}')
                self.broken_proxies.append(proxy)

            logger.info(f'{inspect.stack()[0][3]}: get new proxies')
            self.update_proxies()

        logger.error('Maximum proxies updates exceeded')
        raise Exception('Maximum proxies updates exceeded')

    def imitate_browser_data(self):
        """Create data to fake browser."""
        headers = {**self.headers_base, **{'User-Agent': self.choose_user_agent()}}
        proxy = self.choose_proxy()

        return {
            'headers': headers,
            'proxies': {
                'http': f'http://{proxy[0]}:{proxy[1]}',
                'https': f'http://{proxy[0]}:{proxy[1]}',
            },
            'cookies': imitate_cookie(),
        }

    def get_page(self, url):
        """Try getting page until it's done or until proxies are exceeded."""
        logger.debug(f'Open {url}')

        time.sleep(random.randint(2, 6))

        while True:
            browser_data = self.imitate_browser_data()
            try:
                page = requests.get(url, **browser_data)

                if page.status_code == 200:
                    return page

                logger.info(f'Status code for {self.current_proxy} is {page.status_code}')
                self.broken_proxies.append(self.current_proxy)

            except requests.exceptions.ProxyError as e:
                logger.info(
                    f'{inspect.stack()[0][3]}: proxy error in proxy {self.current_proxy}')
                self.broken_proxies.append(self.current_proxy)
                continue


def imitate_cookie():
    """Create random cookie with random chars and digits."""
    pool = string.ascii_lowercase + string.digits

    base_dict = {
        'ssid': str(random.randint(1000000000, 9999999999)),
        'refreg': f'http:%3A%2F%2Fwww.{"".join([random.choice(string.ascii_lowercase ) for x in range(4, 9)])}.com%2F',
        'GHGHGH': ''.join([random.choice(pool) for x in range(random.randint(20, 32))]),
    }

    random_dict_size = random.randrange(1, 4)

    random_dict = dict(zip(
        [''.join([random.choice(string.ascii_letters) for x in range(random.randrange(3, 6))]) for x in range(random_dict_size)],
        [''.join([random.choice(string.ascii_letters) for x in range(random.randrange(3, 10))]) for x in range(random_dict_size)],
    ))

    return {**base_dict, **random_dict}


def proxy_is_working(proxy):
    """Check that proxy is working properly"""
    checker_site = 'http://ident.me/'

    proxy_ip, proxy_port = proxy

    logger.debug(f'check proxy {proxy_ip, proxy_port}')

    try:
        outer_ip = requests.get(checker_site, proxies={'http': f'http://{proxy_ip}:{proxy_port}'})
    except requests.exceptions.ProxyError:
        logger.info(f'{inspect.stack()[0][3]}: proxy error in proxy {proxy}')
        return False
    return outer_ip.text == proxy_ip


def count_pages(page_soup):
    reviews_count = int(page_soup.find(class_='votes').text)

    return reviews_count // 20


def collect_reviews_urls(page_soup):
    urls_tags = page_soup.find_all(class_='review-title')
    reviews_urls = list(map(lambda x: x.attrs['href'], urls_tags))
    logger.debug(f'reviews links: {" ".join(reviews_urls)}')
    return reviews_urls


def parse_review(review_soup):
    review = {
        'date': review_soup.find("abbr", "value").attrs['title'],
        'rating': review_soup.find(class_="rating").attrs['title'],
        'username': review_soup.find_all(itemprop="name")[1].text,
        'text': review_soup.find(class_='review-body description').text,
        'advantages': review_soup.find(class_='review-plus').text,
        'disadvantages': review_soup.find(class_='review-minus').text,
    }

    return review


def main(get_proxies_from_web):
    booker_url = 'https://otzovik.com/reviews/bukmekerskaya_kontora_liga_stavok/'
    # booker_url = 'https://otzovik.com/reviews/termalnie_istochniki_kalitea_greciya_rodos/'

    browser = Browser(get_proxies_from_web=get_proxies_from_web)

    reviews_urls = []

    parsed_reviews_all = []

    try:
        logger.info('Start parsing')
        first_page = browser.get_page(booker_url)

        first_page_soup = BeautifulSoup(first_page.text, 'html.parser')

        pages_count = count_pages(first_page_soup)

        logger.info(f'Pages count {pages_count}')

        reviews_urls.extend(collect_reviews_urls(first_page_soup))

        for page_number in range(pages_count):
            page_url = f'{booker_url}{page_number + 2}'

            page = browser.get_page(page_url)
            page_soup = BeautifulSoup(page.text, 'html.parser')

            reviews_urls.extend(collect_reviews_urls(page_soup))

        for review_url in reviews_urls:
            logger.debug(f'Parse review {review_url}')
            review = browser.get_page(review_url)

            review_soup = BeautifulSoup(review.text, 'html.parser')

            parsed_reviews = parse_review(review_soup)

            parsed_reviews_all.append(parsed_reviews)

    except Exception as e:
        logger.info(f'During parsing there was an exception {e}')
        traceback.print_exc(file=sys.stdout)

    report = {
        'booker_name': booker_url.strip('/').split('/')[-1],
        'source': booker_url,
        'reviews': parsed_reviews_all,
    }

    with open('report.json', 'w') as fo:
        json.dump(report, fo, ensure_ascii=False)

    logger.info('End parsing')


if __name__ == '__main__':
    try:
        get_proxies_from_web = True if sys.argv[1] == 'web' else False
    except IndexError:
        get_proxies_from_web = True

    main(get_proxies_from_web)
