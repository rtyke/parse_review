### Parser

Simple parser to get reviews from otzovik.

Depends on:

- python 3.6
- requests
- BeautifulSoup
- lxml

```
pip install -r requirements.txt
```

Run with proxy updates from web:
```
python otzovik.py
```

or 

```
python otzovik.py web
```

Run with proxy updates from file `proxies`:

```
python otzovik.py file
```
